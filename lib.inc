%define EXIT_SYSCALL 60
%define WRITE_SYSCALL 1
%define READ_SYSCALL 0
%define STDOUT 1
%define STDIN 0

section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_SYSCALL
    syscall


; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .loop:
        cmp byte [rdi + rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret
    

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdi, STDOUT
    mov rdx, rax
    mov rax, WRITE_SYSCALL
    syscall
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, `\n`


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    mov rdi, STDOUT
    mov rdx, 1
    mov rax, WRITE_SYSCALL
    syscall
    pop rdi
    ret


; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    mov rax, rdi
    test rax, rax
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi


; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax ; quotient
    xor rdx, rdx ; remainder
    
    push rbx
    mov rbx, rsp
    mov rax, rdi
    mov rsi, 10 ; основание 10тичной системы
    mov [rsp], byte 0	    

    .loop:
        div rsi
	lea rdx, ['0' + rdx]
	dec rsp
	mov [rsp], dl		
	xor rdx, rdx
        test rax, rax
        jnz .loop 
    
    mov rdi, rsp
    call print_string
   
    mov rsp, rbx ; restore stack
    pop rbx
    ret


; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
 
    .loop:
        mov al, byte [rdi + rcx]
        cmp al, byte [rsi + rcx]
        jne .ne
        cmp byte [rdi + rcx], 0
	je .eq
	inc rcx
	jmp .loop
    .ne:
        xor rax, rax
        ret
    .eq:
        mov rax, 1
        ret
    
   
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp	
    mov rax, READ_SYSCALL
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, 1
    syscall

    test rax, rax
    je .eof
    
    mov al, byte [rsp]
    inc rsp
    ret 
    .eof:
	inc rsp
        ret 


; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:	
    xor rcx, rcx
    push rbx
    push r14
    push r15
    mov rbx, rdi
    mov r14, rsi
    mov r15, rcx
	
    .wait_not_whitespace:
    	call read_char
	cmp rax, ' '
	je .wait_not_whitespace
	cmp rax, `\t`
	je .wait_not_whitespace
	cmp rax, `\n`
	je .wait_not_whitespace
	jmp .wait_whitespace
	    
    .wait_whitespace:
	cmp rax, ' '
	je .write_terminated_zero	
	cmp rax, `\t`
	je .write_terminated_zero
	cmp rax, `\n`
	je .write_terminated_zero
	test rax, rax
	je .write_terminated_zero

	.write_to_buffer:
	    inc r15
	    cmp r15, r14
	    ja .overflow
	    mov [rbx + r15 - 1], al
	    test rax, rax
	    je .success  
	    jmp .read_char
	.write_terminated_zero:
	    xor rax, rax
	    jmp .write_to_buffer

 	.read_char:
	   call read_char
	
	jmp .wait_whitespace

    .overflow:
	xor rax, rax
	jmp .return
    .success:
	mov rax, rbx
	lea rdx, [r15 - 1]
    .return:
        pop r15
	pop r14
	pop rbx
	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:    
    xor rcx, rcx ; длина
    xor rax, rax ; результирующее число
    mov r8, 10 ; основание десятичной системы

    .loop:
    	xor rsi, rsi
    	mov sil, byte [rdi + rcx]
	cmp sil, '0'
	jb .return
	cmp sil, '9'
	ja .return
	sub rsi, '0'
	mul r8
	add rax, rsi
	inc rcx
	jmp .loop

    .return:
    	mov rdx, rcx
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .negative
    cmp byte[rdi], '+'
    je .positive

    .unsigned:
    	jmp parse_uint 
    .negative:
    	inc rdi
    	call parse_uint
        test rdx, rdx
        jz .ret
	inc rdx
	neg rax
	ret
    .positive:
    	inc rdi
	call parse_uint
        test rdx, rdx
        jz .ret
	inc rdx
    .ret:
	ret
    	     

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:	
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi

    xor rcx, rcx
    inc rax
    cmp rax, rdx
    ja .fail

    .loop:
        cmp rax, rcx
	je .end
	mov dl, byte [rdi + rcx]
	mov [rsi + rcx], dl
	inc rcx
        jmp .loop	
    
    .end:
        ret    

    .fail: 
        xor rax, rax
        ret
